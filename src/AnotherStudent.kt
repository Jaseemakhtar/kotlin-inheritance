class AnotherStudent : AnotherPerson{
    private var studentId: String
    constructor(name: String, age: Int, id: String): super(name, age){
        studentId = id
    }

    fun getId(): String{
        return studentId
    }
}
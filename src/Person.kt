open class Person(name: String, age: Int) {
     private var name: String = ""
     private var age: Int = 0

    init{
        this.name = name
        this.age = age
    }

    fun getName(): String{
        return name
    }

    fun setName(name: String){
        this.name = name
    }

    fun setAge(age: Int){
        this.age = age
    }

    fun getAge(): Int{
        return age
    }
}
fun main(args: Array<String>) {
    var myStudent = Student("Jaseem", 23, "M1513608")
    println(myStudent.getID() + " : " + myStudent.getName() + " : " + myStudent.getAge())
    var anotherStudent = AnotherStudent("Tony",25,"M151")
    println("${anotherStudent.getId()} : ${anotherStudent.getName()} : ${anotherStudent.getAge()}")
}
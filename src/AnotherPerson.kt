open class AnotherPerson{
    private var name: String
    private var age: Int

    constructor(name: String, age: Int){
        this.name = name
        this.age = age
    }

    fun getName(): String{
        return name
    }

    fun getAge(): Int{
        return age
    }
}